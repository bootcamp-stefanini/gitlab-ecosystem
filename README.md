<div align="center">

<p>
  <img alt="logo" src="https://v2.helm.sh/img/boat.gif" width="350px" float="center"/>
</p>

<h2 align="center">✨ GitLab Playground ✨</h2>

<div align="center">

[![Semantic Release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://gitlab.com/dry-group/cluster-management)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](https://gitlab.com/dry-group/cluster-management)

</div>

---

<p align="center">
  <img alt="funny-gif" src="https://cdn.dribbble.com/users/1480650/screenshots/4739771/autodevops-dribbble-gif.gif" width="450px" float="center"/>
</p>

<p align="center">  
  ✨ Using Docker and Docker-Compose to create a GitLab Playground on your local machine ✨
</p>

<p align="center">
  <a href="#getting-started">Getting Started</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#versioning">Versioning</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#contributing">Contributing</a>
</p>

</div>

---

## ➤ Getting Started <a name = "getting-started"></a>

If you want contribute on this project, first you need to make a **git clone**:

>
> 1. git clone --depth 1 <https://gitlab.com/bootcamp-stefanini/gitlab-ecosystem.git> -b main
>

This will give you access to the code on your **local machine**.

## ➤ Prerequisites <a name = "prerequisites"></a>

Before you start developing on this project you need to install some tools on your **local machine**:

### Docker tools  

- **docker**, **docker-compose**

### Support tools 

- **npm**, **make**, **gitleaks**

For more information, access the [CONTRIBUTING](CONTRIBUTING.md) file.

## ➤ Installing <a name = "installing"></a>

### Support dependencies

To install support dependencies that handle commit and release standards, run the command:

```bash
npm install
```

This shareable configuration use the following dependencies:

- ⮚ [Semantic Release](https://github.com/semantic-release) + Plugins de configuração
  - [`semantic-release`](https://github.com/semantic-release/semantic-release)
  - [`@semantic-release/git`](https://github.com/semantic-release/git)
  - [`@semantic-release/github`](https://github.com/semantic-release/github)
  - [`@semantic-release/changelog`](https://github.com/semantic-release/changelog)
  - [`@semantic-release/commit-analyzer`](https://github.com/semantic-release/commit-analyzer)
  - [`@semantic-release/release-notes-generator`](https://github.com/semantic-release/release-notes-generator)
- ⮚ [Commit Lint](https://github.com/conventional-changelog/commitlint) usando o [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).
  - [`commitizen`](https://github.com/commitizen/cz-cli)
  - [`@commitlint/cli`](https://github.com/conventional-changelog/commitlint)
  - [`@commitlint/config-conventional`](https://github.com/conventional-changelog/commitlint)
- ⮚ Git Hooks com [Husky](https://github.com/typicode/husky).
  - [`husky`](https://github.com/semantic-release/git)

For more information, access the [CONTRIBUTING](CONTRIBUTING.md) file.

### Git Hooks

Configure husky `commit-msg` and `pre-commit` hooks:

```bash
bash scripts/hooks.sh
```

## ➤ Description <a name = "description"></a>

<div align="center">

<p align="center">
  <img alt="gif-description" src="https://media3.giphy.com/media/5ziQ5kjh4lgCd29WOR/200w.gif?cid=82a1493b0uufgvz9tw6byen3bed3y7h9u0ilzpcuthz7a5ne&rid=200w.gif" width="250px" float="center"/>
</p>

</div>

A ideia desse repositório é que qualquer pessoa que possui o Docker e o Docker Compose instalados em sua máquina local consiga montar um ambiente de playground do GitLab.

<div align="center">

<p align="center">
  <img alt="gif-description" src="https://docs.gitlab.com/ee/development/img/architecture_simplified.png" width="550px" float="center"/>
</p>

</div>

No final teremos os containers do GitLab, GitLab Runner e Minio rodando para simular o mesmo ambiente que temos em nossa estrutura de Cloud.

```txt
                                          +-------------+    +-------------+    +-------------+
                                          |             |    |             |    /             /
                                          |             |    |             |    /             /
                                          |  Gitlab     |    |  Gitlab     |    /  Gitlab CI  /
                                          |             +--> |  CI-Runner  +--> /  Container  /
                                          |             |    |             |    /             /
                                          |             |    |             |    /             /
                                          +------+------+    +------+------+    +------+------+
                                          +------|------------------|------------------|------+
                                          |                      Docker                       |
                                          +---------------------------------------------------+
```

## ➤ Steps <a name = "steps"></a>

Comando a serem executados para criar a demonstração:

```bash
make init
sleep 45
make register
make log
```

## ➤ Versioning <a name = "versioning"></a>

To check the change history, please access the [**CHANGELOG.md**](CHANGELOG.md) file.

## ➤ Contributing <a name = "contributing"></a>

Contributions, issues and feature requests are welcome. Feel free to check issues page if you want to contribute. [Check the contributing guide](CONTRIBUTING.md).

## ➤ Troubleshooting <a name = "troubleshooting"></a>

If you have any problems, please contact **DevOps Team**.

## ➤ Show your support <a name = "show-your-support"></a>

Give a ⭐️ if this project helped you!

---

Made with 💜 by **DevOps Team** :wave: inspired on [readme-md-generator](https://github.com/kefranabg/readme-md-generator)
